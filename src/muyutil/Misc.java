/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package muyutil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.NumberFormat;

public class Misc {
    public static String euro(double valor) {
    NumberFormat nf = NumberFormat.getCurrencyInstance();
    return nf.format(valor);
    }
    public static String posa2d(double valor) {
    NumberFormat nf = NumberFormat.getNumberInstance();
    nf.setGroupingUsed(true);
    nf.setMinimumIntegerDigits(4);
    return nf.format(valor);
    }
    public static double leeNumero(String mensaje) throws IOException {
        double numero = -1;
        try {
            numero = Double.parseDouble(leeCadena(mensaje));
        } 
        catch (NumberFormatException e) {
        }
        return numero;
    }
    public static String leeCadena(String mensaje) throws IOException {
        System.out.println(mensaje);
        BufferedReader lector = new BufferedReader(new InputStreamReader(System.in));
        return lector.readLine();
    }
    public static void main(String[] args) throws IOException {
        double num;
        do {
        num = Misc.leeNumero("entra un valor numérico");
        System.out.println("hay introducido " + Misc.euro(num)+ " " + Misc.posa2d(num));
        } while (num > 0);
    }
}